COB is an LLC created to provide Weight Loss Surgery services to central and Southeastern Ohio. COB functions under the auspices of Central Ohio General Surgeons, under whom I am a physician employee. COB is otherwise independent from this practice.

Address: 2405 N Columbus St, Suite 250, Lancaster, OH 43130, USA

Phone: 740-475-0442
